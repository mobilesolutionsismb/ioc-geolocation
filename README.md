# Geolocation Module

This module exposes a Data Source and Data Provider for handling geolocation with Redux state.  
Works with browser and cordova, and should also work on Windows UWP applications.

This module is part of the [I-REACT Open Core](https://mobilesolutionsismb.bitbucket.io/i-react-open-core/)

## Acknowledgment

This work was partially funded by the European Commission through the [I-REACT project](http://www.i-react.eu/) (H2020-DRS-1-2015), grant agreement n.700256.

### Build

```bash
    npm i
    npm run build
    npm run build-debug
```

Postinstall script coming soon!

#### Run the example

```bash
    cd example
    npm i
    npm link ../
    npm start
```

## Usage

```javascript
//....
import { Reducer } from 'ioc-geolocation';
app.registerReducer('geolocation', geolocation);
//or
import geolocation from 'ioc-geolocation';
app.register(geolocation);
```

### Reducer

If you prefer reducer only

```javascript
import { Reducer } from 'ioc-geolocation';
```

### Data Provider

#### v0.2.0+

```javascript
import { withGeolocation } from 'ioc-geolocation';
let myHoc = withGeolocation(<MySimpleComponent />);
//now you can access position, e.g. from inside the component
this.props.geolocationPosition;
```

## Props exposed by this module

**geolocationPosition** _Geoposition_ position as returned by browser/device

**geolocationUpdating** _Bool_ geolocation is updating

**geolocationError** _Error_

**geolocationWatching** _Bool_ geolocation is in watching mode

**updatePosition** _Function_ update position

**startWatchingPosition** _Function_ triggers geolocation watching

**stopWatchingPosition** _Function_ stop geolocation watching

**updateSettings** _Function_ update geolocation settings

**restoreDefaultSettings** _Function_ reset geolocation settings

## Helpers

It also exposes a handy helper, geolocationErrorTranslations, for getting human-readable translations of geolocation errors

```javascript
import { geolocationErrorTranslations } from 'ioc-geolocation';

//Define a translation function like

function translationFunction(key, defaultText) {
  //will return a default key, useful for instanc with ismb-react-redux-locale
  //and a defaultText which is an english translation of the error
}

//...
if (this.props.geolocationError) {
  let errorMessage = geolocationErrorTranslations(positionError, translationFunction);
  //do what you need with the above string
}
```

### Error keys

Set translations for these keys in your dictionary/dictionaries

Browser/Cordova

- \_position_error_reason_permission
- \_position_error_reason_pos_unavailable
- \_position_error_reason_timeout
- \_position_error_reason_default

Native Windows (UWP)

- \_win_position_error_initializing
- \_win_position_error_noData
- \_win_position_error_disabled
- \_win_position_error_notInitialized
- \_win_position_error_notAvailable
- \_win_position_error_unknown
