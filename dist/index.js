(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("redux-reduce-with"), require("react-redux"), require("reselect"), require("prop-types"));
	else if(typeof define === 'function' && define.amd)
		define(["redux-reduce-with", "react-redux", "reselect", "prop-types"], factory);
	else if(typeof exports === 'object')
		exports["ismb-react-redux-geolocation"] = factory(require("redux-reduce-with"), require("react-redux"), require("reselect"), require("prop-types"));
	else
		root["ismb-react-redux-geolocation"] = factory(root["redux-reduce-with"], root["react-redux"], root["reselect"], root["prop-types"]);
})(window, function(__WEBPACK_EXTERNAL_MODULE_redux_reduce_with__, __WEBPACK_EXTERNAL_MODULE_react_redux__, __WEBPACK_EXTERNAL_MODULE_reselect__, __WEBPACK_EXTERNAL_MODULE_prop_types__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./dataProviders/withGeolocation.js":
/*!******************************************!*\
  !*** ./dataProviders/withGeolocation.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.geolocationActionCreators = exports.geolocationSelector = undefined;

var _reactRedux = __webpack_require__(/*! react-redux */ "react-redux");

var _reselect = __webpack_require__(/*! reselect */ "reselect");

var _ActionCreators = __webpack_require__(/*! ../dataSources/ActionCreators */ "./dataSources/ActionCreators.js");

var ActionCreators = _interopRequireWildcard(_ActionCreators);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var geolocationMapState = function geolocationMapState(state) {
  return state.geolocation.currentPosition;
};
var geolocationUpdatingMapState = function geolocationUpdatingMapState(state) {
  return state.geolocation.updatingPosition;
};
var geolocationErrorMapState = function geolocationErrorMapState(state) {
  return state.geolocation.positionError;
};
var geolocationWatchingMapState = function geolocationWatchingMapState(state) {
  return state.geolocation.watchingPosition;
};
var geolocationSettingsMapState = function geolocationSettingsMapState(state) {
  return state.geolocation.settings;
};

var geolocationSelector = exports.geolocationSelector = (0, _reselect.createStructuredSelector)({
  geolocationPosition: geolocationMapState,
  geolocationUpdating: geolocationUpdatingMapState,
  geolocationError: geolocationErrorMapState,
  geolocationWatching: geolocationWatchingMapState,
  geolocationSettings: geolocationSettingsMapState
});

exports.geolocationActionCreators = ActionCreators;
exports.default = (0, _reactRedux.connect)(geolocationSelector, ActionCreators);

/***/ }),

/***/ "./dataSources/ActionCreators.js":
/*!***************************************!*\
  !*** ./dataSources/ActionCreators.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.restoreDefaultGeolocationSettings = exports.updateGeolocationSettings = exports.stopWatchingPosition = exports.startWatchingPosition = exports.updatePosition = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _DefaultState = __webpack_require__(/*! ./DefaultState */ "./dataSources/DefaultState.js");

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(/*! ./Actions */ "./dataSources/Actions.js");

var _isWindows = __webpack_require__(/*! ../isWindows */ "./isWindows.js");

var _geolocationErrorTranslations = __webpack_require__(/*! ../geolocationErrorTranslations */ "./geolocationErrorTranslations.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//A geolocator instance https://msdn.microsoft.com/library/windows/apps/windows.devices.geolocation.aspx
var windowsDevicesLocator = null;
var windowsPositionWatching = false;
//will hold the location change and status handler
var windowsPositionChangedHandler = null;
var windowsPositionStatusHandler = null;

var EnhancedPositionError = function (_Error) {
    _inherits(EnhancedPositionError, _Error);

    function EnhancedPositionError(positionError) {
        _classCallCheck(this, EnhancedPositionError);

        var _this = _possibleConstructorReturn(this, (EnhancedPositionError.__proto__ || Object.getPrototypeOf(EnhancedPositionError)).call(this, positionError.message));

        _this.code = positionError.code;
        _this.translatedMessaged = (0, _geolocationErrorTranslations.geolocationErrorTranslations)(positionError);
        return _this;
    }

    _createClass(EnhancedPositionError, [{
        key: 'toString',
        value: function toString() {
            return this.translatedMessaged;
        }
    }]);

    return EnhancedPositionError;
}(Error);

function initWindowsDeviceLocator(settings) {
    if (windowsDevicesLocator === null) {
        windowsDevicesLocator = new Windows.Devices.Geolocation.Geolocator();
        windowsDevicesLocator.desiredAccuracy = settings.desiredAccuracy;
        windowsDevicesLocator.movementThreshold = settings.movementThreshold;
        windowsDevicesLocator.reportInterval = settings.reportInterval;
    }
}

//Get Current Position
async function getPosition(settings) {
    return new Promise(function (success, fail) {
        var done = function done(geolocation) {
            var coords = geolocation.coords,
                timestamp = geolocation.timestamp; //make a pojo

            var accuracy = coords.accuracy,
                altitude = coords.altitude,
                altitudeAccuracy = coords.altitudeAccuracy,
                heading = coords.heading,
                latitude = coords.latitude,
                longitude = coords.longitude,
                speed = coords.speed;

            success({
                coords: {
                    accuracy: accuracy,
                    altitude: altitude,
                    altitudeAccuracy: altitudeAccuracy,
                    heading: heading,
                    latitude: latitude,
                    longitude: longitude,
                    speed: speed
                },
                timestamp: timestamp
            });
        };

        if ((0, _isWindows.isNativeWindows)()) {
            initWindowsDeviceLocator(settings);
            windowsDevicesLocator.getGeopositionAsync().done(done, fail);
        } else {
            navigator.geolocation.getCurrentPosition(done, fail, settings);
        }
    });
}

function updatePosition() {
    return async function (dispatch, getState) {
        var geolocationState = getState().geolocation;
        dispatch({
            type: _Actions.POSITION_UPDATING
        });
        var position = null;
        try {
            position = await getPosition(geolocationState.settings);
            dispatch({
                type: _Actions.POSITION_UPDATED,
                currentPosition: position
            });
            return position;
        } catch (positionError) {
            console.log('updatePosition ERR', positionError);
            var err = new EnhancedPositionError(positionError);
            dispatch({
                type: _Actions.POSITION_ERROR,
                positionError: err
            });
            throw err;
        }
    };
}

//Position Watching
var positionWatchingHandle = null;

function watchPositionHandler(dispatch) {
    return function (geolocation) {
        var coords = geolocation.coords,
            timestamp = geolocation.timestamp; //make a pojo

        var accuracy = coords.accuracy,
            altitude = coords.altitude,
            altitudeAccuracy = coords.altitudeAccuracy,
            heading = coords.heading,
            latitude = coords.latitude,
            longitude = coords.longitude,
            speed = coords.speed;


        dispatch({
            type: _Actions.POSITION_UPDATED,
            currentPosition: {
                coords: {
                    accuracy: accuracy,
                    altitude: altitude,
                    altitudeAccuracy: altitudeAccuracy,
                    heading: heading,
                    latitude: latitude,
                    longitude: longitude,
                    speed: speed
                },
                timestamp: timestamp
            }
        });
    };
}

function watchPositionErrorHandler(dispatch) {
    return function (positionError) {
        var err = new EnhancedPositionError(positionError);
        dispatch({
            type: _Actions.POSITION_ERROR,
            positionError: err
        });
    };
}

function _windowsPositionChangedHandler(dispatch) {
    return function (positionFromSensors) {
        var position = {};
        if (positionFromSensors.position && positionFromSensors.position.coordinate) {
            //translate
            var winPosition = positionFromSensors.position.coordinate;
            position['coords'] = {};
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = Object.keys(winPosition)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var f = _step.value;

                    position.coords[f] = winPosition[f];
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        } else {
            position = positionFromSensors;
        }
        dispatch({
            type: _Actions.POSITION_UPDATED,
            currentPosition: position
        });
    };
}

function _windowsPositionStatusHandler(dispatch) {
    return function (positionStatus) {
        var status = positionStatus.status;
        if (status !== Windows.Devices.Geolocation.PositionStatus.ready) {
            var err = new EnhancedPositionError({ code: status.code, message: 'Position Error' });
            dispatch({
                type: _Actions.POSITION_ERROR,
                positionError: err
            });
        }
    };
}

function startWatchingPosition() {
    return async function (dispatch, getState) {
        return new Promise(function (success) {
            var geolocationState = getState().geolocation;
            if ((0, _isWindows.isNativeWindows)()) {
                initWindowsDeviceLocator(geolocationState.settings);
                if (!windowsPositionWatching) {
                    windowsPositionChangedHandler = _windowsPositionChangedHandler(dispatch);
                    windowsPositionStatusHandler = _windowsPositionStatusHandler(dispatch);
                    windowsDevicesLocator.addEventListener('positionchanged', windowsPositionChangedHandler);
                    windowsDevicesLocator.addEventListener('statuschanged', windowsPositionStatusHandler);
                    windowsPositionWatching = true;
                }
            } else {
                if (positionWatchingHandle === null) {
                    positionWatchingHandle = navigator.geolocation.watchPosition(watchPositionHandler(dispatch), watchPositionErrorHandler(dispatch), geolocationState.settings);
                    dispatch({
                        type: _Actions.WATCHING_POSITION_START
                    });
                }
            }
            success();
        });
    };
}

function stopWatchingPosition() {
    return async function (dispatch) {
        return new Promise(function (success) {
            if ((0, _isWindows.isNativeWindows)()) {
                if (windowsPositionWatching) {
                    windowsDevicesLocator.removeEventListener('positionchanged', windowsPositionChangedHandler);
                    windowsDevicesLocator.removeEventListener('statuschanged', windowsPositionStatusHandler);
                    windowsPositionChangedHandler = null;
                    windowsPositionStatusHandler = null;
                    windowsPositionWatching = false;
                }
            } else {
                if (positionWatchingHandle !== null) {
                    navigator.geolocation.clearWatch(positionWatchingHandle);
                    positionWatchingHandle = null;
                    dispatch({
                        type: _Actions.WATCHING_POSITION_STOP
                    });
                }
            }
            success();
        });
    };
}

function restoreDefaultSettingsAction() {
    return {
        type: _Actions.RESET_SETTINGS
    };
}

function isNumber(val) {
    return typeof val === 'number' && !isNaN(val);
}

function isPositiveNumber(val) {
    return isNumber(val) && val > 0;
}

function updateSettingsAction(settings, currentSettings) {
    var _settings = void 0;
    if ((0, _isWindows.isNativeWindows)()) {
        var reportInterval = settings.reportInterval,
            movementThreshold = settings.movementThreshold,
            desiredAccuracy = settings.desiredAccuracy;

        _settings = {
            reportInterval: isPositiveNumber(reportInterval) ? reportInterval : currentSettings.reportInterval,
            movementThreshold: isPositiveNumber(movementThreshold) ? movementThreshold : currentSettings.movementThreshold,
            desiredAccuracy: desiredAccuracy === Windows.Devices.Geolocation.PositionAccuracy.high || desiredAccuracy === Windows.Devices.Geolocation.PositionAccuracy.low ? desiredAccuracy : currentSettings.desiredAccuracy
        };
    } else {
        var enableHighAccuracy = settings.enableHighAccuracy,
            timeout = settings.timeout,
            maximumAge = settings.maximumAge;

        _settings = {
            enableHighAccuracy: typeof enableHighAccuracy === 'boolean' ? enableHighAccuracy : currentSettings.enableHighAccuracy,
            timeout: isPositiveNumber(timeout) ? timeout : currentSettings.timeout,
            maximumAge: isPositiveNumber(maximumAge) ? maximumAge : currentSettings.maximumAge
        };
    }
    return {
        type: _Actions.UPDATE_SETTINGS,
        settings: _settings
    };
}

function updateGeolocationSettings(settings) {
    return async function (dispatch, getState) {
        var geolocationState = getState().geolocation;
        var nextSettings = updateSettingsAction(settings, geolocationState.settings);
        if (geolocationState.watchingPosition) {
            await dispatch(stopWatchingPosition());
            dispatch(nextSettings);
            await dispatch(startWatchingPosition());
        } else {
            dispatch(nextSettings);
        }
    };
}

function restoreDefaultGeolocationSettings() {
    return async function (dispatch, getState) {
        var geolocationState = getState().geolocation;
        var defaultSettings = restoreDefaultSettingsAction();
        if (geolocationState.watchingPosition) {
            await dispatch(stopWatchingPosition());
            dispatch(defaultSettings);
            await dispatch(startWatchingPosition());
        } else {
            dispatch(defaultSettings);
        }
    };
}

exports.updatePosition = updatePosition;
exports.startWatchingPosition = startWatchingPosition;
exports.stopWatchingPosition = stopWatchingPosition;
exports.updateGeolocationSettings = updateGeolocationSettings;
exports.restoreDefaultGeolocationSettings = restoreDefaultGeolocationSettings;

/***/ }),

/***/ "./dataSources/Actions.js":
/*!********************************!*\
  !*** ./dataSources/Actions.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var NS = 'GEOLOCATION';

var POSITION_UPDATED = NS + '@POSITION_UPDATED';
var POSITION_UPDATING = NS + '@POSITION_UPDATING';
var POSITION_ERROR = NS + '@POSITION_ERROR';
var WATCHING_POSITION_START = NS + '@WATCHING_POSITION_START';
var WATCHING_POSITION_STOP = NS + '@WATCHING_POSITION_STOP';
var UPDATE_SETTINGS = NS + '@UPDATE_SETTINGS';
var RESET_SETTINGS = NS + '@RESET_SETTINGS';

exports.POSITION_UPDATED = POSITION_UPDATED;
exports.POSITION_UPDATING = POSITION_UPDATING;
exports.POSITION_ERROR = POSITION_ERROR;
exports.WATCHING_POSITION_START = WATCHING_POSITION_START;
exports.WATCHING_POSITION_STOP = WATCHING_POSITION_STOP;
exports.UPDATE_SETTINGS = UPDATE_SETTINGS;
exports.RESET_SETTINGS = RESET_SETTINGS;

/***/ }),

/***/ "./dataSources/DefaultState.js":
/*!*************************************!*\
  !*** ./dataSources/DefaultState.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _isWindows = __webpack_require__(/*! ../isWindows */ "./isWindows.js");

var NULL_POSITION = {
    coords: {
        accuracy: 100000,
        altitude: null,
        altitudeAccuracy: null,
        heading: null,
        latitude: 0,
        longitude: 0,
        speed: null
    },
    timestamp: 0
};

var WINDOWS_DEVICES_MOVEMENT_THRESHOLD = 3; //meters
var WATCH_POSITION_TIMEOUT = 10000;

var DEFAULT_GEOLOCATION_SETTINGS = {
    enableHighAccuracy: true,
    timeout: WATCH_POSITION_TIMEOUT,
    maximumAge: 30 * 1000
};

var IS_WINDOWS = (0, _isWindows.isNativeWindows)();
if (IS_WINDOWS) {
    DEFAULT_GEOLOCATION_SETTINGS = {
        reportInterval: WATCH_POSITION_TIMEOUT,
        movementThreshold: WINDOWS_DEVICES_MOVEMENT_THRESHOLD,
        desiredAccuracy: Windows.Devices.Geolocation.PositionAccuracy.high
    };
}

//Default State
var STATE = {
    currentPosition: NULL_POSITION,
    positionError: null,
    updatingPosition: false,
    watchingPosition: false,
    settings: DEFAULT_GEOLOCATION_SETTINGS
};

exports.default = STATE;

/***/ }),

/***/ "./dataSources/Reducer.js":
/*!********************************!*\
  !*** ./dataSources/Reducer.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mutators;

var _reduxReduceWith = __webpack_require__(/*! redux-reduce-with */ "redux-reduce-with");

var _DefaultState = __webpack_require__(/*! ./DefaultState */ "./dataSources/DefaultState.js");

var _DefaultState2 = _interopRequireDefault(_DefaultState);

var _Actions = __webpack_require__(/*! ./Actions */ "./dataSources/Actions.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var mutators = (_mutators = {}, _defineProperty(_mutators, _Actions.POSITION_UPDATED, {
  currentPosition: function currentPosition(action) {
    return action.currentPosition;
  },
  updatingPosition: false,
  positionError: null
}), _defineProperty(_mutators, _Actions.POSITION_UPDATING, {
  updatingPosition: true
}), _defineProperty(_mutators, _Actions.POSITION_ERROR, {
  // currentPosition: DefaultState.currentPosition,
  updatingPosition: false,
  positionError: function positionError(action) {
    return action.positionError;
  }
}), _defineProperty(_mutators, _Actions.WATCHING_POSITION_START, {
  watchingPosition: true
}), _defineProperty(_mutators, _Actions.WATCHING_POSITION_STOP, {
  watchingPosition: false
}), _defineProperty(_mutators, _Actions.UPDATE_SETTINGS, {
  settings: function settings(action) {
    return action.settings;
  }
}), _defineProperty(_mutators, _Actions.RESET_SETTINGS, {
  settings: function settings(action) {
    return _DefaultState2.default.settings;
  }
}), _mutators);

exports.default = (0, _reduxReduceWith.reduceWith)(mutators, _DefaultState2.default);

/***/ }),

/***/ "./geolocationErrorTranslations.js":
/*!*****************************************!*\
  !*** ./geolocationErrorTranslations.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
var IS_WINDOWS = typeof cordova !== 'undefined' && cordova.platformId === 'windows' && typeof Windows !== 'undefined';

function defaultTraslate(key, defaultText) {
    return defaultText;
}

function geolocationErrorTranslations(positionError) {
    var translate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultTraslate;


    function _getPositionErrorReasonWindows(code) {
        var reason = '';
        switch (code) {
            //   case Windows.Devices.Geolocation.PositionStatus.ready:
            //    // Location data is available
            //    reason = translate('win_position_ready','Location is available.');
            //    break;
            case Windows.Devices.Geolocation.PositionStatus.initializing:
                // This status indicates that a GPS is still acquiring a fix
                reason = translate('_win_position_error_initializing', 'A GPS device is still initializing');
                break;
            case Windows.Devices.Geolocation.PositionStatus.noData:
                // No location data is currently available
                reason = translate('_win_position_error_noData', 'Data from location services is currently unavailable');
                break;
            case Windows.Devices.Geolocation.PositionStatus.disabled:
                // The app doesn't have permission to access location,
                // either because location has been turned off.
                reason = translate('_win_position_error_disabled', 'Your location is currently turned off. ' + 'Change your settings through the Settings charm ' + ' to turn it back on');
                break;
            case Windows.Devices.Geolocation.PositionStatus.notInitialized:
                // This status indicates that the app has not yet requested
                // location data by calling GetGeolocationAsync() or
                // registering an event handler for the positionChanged event.
                reason = translate('_win_position_error_notInitialized', 'Location status is not initialized because ' + 'the app has not requested location data');
                break;
            case Windows.Devices.Geolocation.PositionStatus.notAvailable:
                // Location is not available on this version of Windows
                reason = translate('_win_position_error_notAvailable', 'You do not have the required location services ' + 'present on your system');
                break;
            default:
                reason = translate('_win_position_error_unknown', 'Unknown Position Status');
                break;
        }
        return reason;
    }

    function _getPositionErrorReason(positionError) {
        var code = positionError.code,
            message = positionError.message;

        message = message.length > 0 ? message : null;
        var reason = '';
        switch (code) {
            case 1:
                //PERMISSION DENIED
                reason = translate('_position_error_reason_permission', message || 'Permission denied. Check location access permission is enabled in your system settings. On Google Chrome, also check that your origin is secure (https://)');
                break;
            case 2:
                //POSITION UNAVAILABLE
                reason = translate('_position_error_reason_pos_unavailable', message || 'Position unavailable. Ensure your location sensor is enabled or try to move to a different place.');
                break;
            case 3:
                //TIMEOUT
                reason = translate('_position_error_reason_timeout', message || 'Position detection timeout.');
                break;
            default:
                reason = translate('_position_error_reason_default', message || 'Ensure your location sensor is enabled and/or you are connected to the internet.');
                break;
        }
        return reason;
    }

    //Windows devices
    if (IS_WINDOWS) {
        return _getPositionErrorReasonWindows(positionError.code);
    } else {
        //non windows or browser
        return _getPositionErrorReason(positionError);
    }
}

exports.geolocationErrorTranslations = geolocationErrorTranslations;
exports.default = geolocationErrorTranslations;

/***/ }),

/***/ "./index.js":
/*!******************!*\
  !*** ./index.js ***!
  \******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.geolocationSelector = exports.geolocationActionCreators = exports.GeolocationType = exports.geolocationErrorTranslations = exports.withGeolocation = exports.Reducer = undefined;

var _ActionCreators = __webpack_require__(/*! ./dataSources/ActionCreators */ "./dataSources/ActionCreators.js");

Object.keys(_ActionCreators).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _ActionCreators[key];
    }
  });
});

var _geolocationErrorTranslations = __webpack_require__(/*! ./geolocationErrorTranslations */ "./geolocationErrorTranslations.js");

var _geolocationErrorTranslations2 = _interopRequireDefault(_geolocationErrorTranslations);

var _Reducer = __webpack_require__(/*! ./dataSources/Reducer */ "./dataSources/Reducer.js");

var _Reducer2 = _interopRequireDefault(_Reducer);

var _withGeolocation = __webpack_require__(/*! ./dataProviders/withGeolocation */ "./dataProviders/withGeolocation.js");

var _withGeolocation2 = _interopRequireDefault(_withGeolocation);

var _propTypes = __webpack_require__(/*! prop-types */ "prop-types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (!navigator.geolocation) {
  throw new Error('Your browser/device does not support geolocation');
}

var coordsShape = {
  accuracy: _propTypes.PropTypes.number,
  altitude: _propTypes.PropTypes.number,
  altitudeAccuracy: _propTypes.PropTypes.number,
  heading: _propTypes.PropTypes.number,
  latitude: _propTypes.PropTypes.number,
  longitude: _propTypes.PropTypes.number,
  speed: _propTypes.PropTypes.number
};

var GeolocationShape = {
  coords: _propTypes.PropTypes.shape(coordsShape),
  timestamp: _propTypes.PropTypes.number
};

//Useful for type checking
var GeolocationType = _propTypes.PropTypes.shape(GeolocationShape);

exports.default = {
  name: 'geolocation',
  reducer: _Reducer2.default
};
exports.Reducer = _Reducer2.default;
exports.withGeolocation = _withGeolocation2.default;
exports.geolocationErrorTranslations = _geolocationErrorTranslations2.default;
exports.GeolocationType = GeolocationType;
exports.geolocationActionCreators = _withGeolocation.geolocationActionCreators;
exports.geolocationSelector = _withGeolocation.geolocationSelector;

/***/ }),

/***/ "./isWindows.js":
/*!**********************!*\
  !*** ./isWindows.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
function isNativeWindows() {
    return typeof cordova !== 'undefined' && cordova.platformId === 'windows' && typeof Windows !== 'undefined';
}

exports.isNativeWindows = isNativeWindows;
exports.default = isNativeWindows;

/***/ }),

/***/ 0:
/*!************************!*\
  !*** multi ./index.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/vito/Development/JavaScript/IOC/ioc-geolocation/src/index.js */"./index.js");


/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_prop_types__;

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_react_redux__;

/***/ }),

/***/ "redux-reduce-with":
/*!************************************!*\
  !*** external "redux-reduce-with" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_redux_reduce_with__;

/***/ }),

/***/ "reselect":
/*!***************************!*\
  !*** external "reselect" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_reselect__;

/***/ })

/******/ });
});
//# sourceMappingURL=index.js.map