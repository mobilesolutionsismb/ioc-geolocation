import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import * as ActionCreators from '../dataSources/ActionCreators';

const geolocationMapState = state => state.geolocation.currentPosition;
const geolocationUpdatingMapState = state => state.geolocation.updatingPosition;
const geolocationErrorMapState = state => state.geolocation.positionError;
const geolocationWatchingMapState = state => state.geolocation.watchingPosition;
const geolocationSettingsMapState = state => state.geolocation.settings;

export const geolocationSelector = createStructuredSelector({
  geolocationPosition: geolocationMapState,
  geolocationUpdating: geolocationUpdatingMapState,
  geolocationError: geolocationErrorMapState,
  geolocationWatching: geolocationWatchingMapState,
  geolocationSettings: geolocationSettingsMapState
});

export { ActionCreators as geolocationActionCreators };

export default connect(
  geolocationSelector,
  ActionCreators
);
