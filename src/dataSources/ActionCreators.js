import DefaultState from './DefaultState';
import { POSITION_UPDATED, POSITION_UPDATING, POSITION_ERROR, WATCHING_POSITION_START, WATCHING_POSITION_STOP, UPDATE_SETTINGS, RESET_SETTINGS } from './Actions';
import { isNativeWindows } from '../isWindows';
import { geolocationErrorTranslations } from '../geolocationErrorTranslations';
//A geolocator instance https://msdn.microsoft.com/library/windows/apps/windows.devices.geolocation.aspx
let windowsDevicesLocator = null;
let windowsPositionWatching = false;
//will hold the location change and status handler
let windowsPositionChangedHandler = null;
let windowsPositionStatusHandler = null;

class EnhancedPositionError extends Error {
    constructor(positionError) {
        super(positionError.message);
        this.code = positionError.code;
        this.translatedMessaged = geolocationErrorTranslations(positionError);
    }

    toString() {
        return this.translatedMessaged;
    }
}


function initWindowsDeviceLocator(settings) {
    if (windowsDevicesLocator === null) {
        windowsDevicesLocator = new Windows.Devices.Geolocation.Geolocator();
        windowsDevicesLocator.desiredAccuracy = settings.desiredAccuracy;
        windowsDevicesLocator.movementThreshold = settings.movementThreshold;
        windowsDevicesLocator.reportInterval = settings.reportInterval;
    }
}

//Get Current Position
async function getPosition(settings) {
    return new Promise((success, fail) => {
        const done = (geolocation) => {
            let { coords, timestamp } = geolocation; //make a pojo
            let { accuracy, altitude, altitudeAccuracy, heading, latitude, longitude, speed } = coords;
            success({
                coords: {
                    accuracy,
                    altitude,
                    altitudeAccuracy,
                    heading,
                    latitude,
                    longitude,
                    speed
                },
                timestamp
            });
        };

        if (isNativeWindows()) {
            initWindowsDeviceLocator(settings);
            windowsDevicesLocator.getGeopositionAsync().done(done, fail);
        } else {
            navigator.geolocation.getCurrentPosition(done, fail, settings);
        }
    });
}

function updatePosition() {
    return async (dispatch, getState) => {
        let geolocationState = getState().geolocation;
        dispatch({
            type: POSITION_UPDATING
        });
        let position = null;
        try {
            position = await getPosition(geolocationState.settings);
            dispatch({
                type: POSITION_UPDATED,
                currentPosition: position
            });
            return position;
        } catch (positionError) {
            console.log('updatePosition ERR', positionError);
            const err = new EnhancedPositionError(positionError);
            dispatch({
                type: POSITION_ERROR,
                positionError: err
            });
            throw err;
        }
    };
}

//Position Watching
let positionWatchingHandle = null;

function watchPositionHandler(dispatch) {
    return geolocation => {
        let { coords, timestamp } = geolocation; //make a pojo
        let { accuracy, altitude, altitudeAccuracy, heading, latitude, longitude, speed } = coords;

        dispatch({
            type: POSITION_UPDATED,
            currentPosition: {
                coords: {
                    accuracy,
                    altitude,
                    altitudeAccuracy,
                    heading,
                    latitude,
                    longitude,
                    speed
                },
                timestamp
            }
        });
    };
}

function watchPositionErrorHandler(dispatch) {
    return positionError => {
        const err = new EnhancedPositionError(positionError);
        dispatch({
            type: POSITION_ERROR,
            positionError: err
        });
    };
}

function _windowsPositionChangedHandler(dispatch) {
    return positionFromSensors => {
        let position = {};
        if (positionFromSensors.position && positionFromSensors.position.coordinate) {
            //translate
            let winPosition = positionFromSensors.position.coordinate;
            position['coords'] = {};
            for (let f of Object.keys(winPosition)) {
                position.coords[f] = winPosition[f];
            }
        } else {
            position = positionFromSensors;
        }
        dispatch({
            type: POSITION_UPDATED,
            currentPosition: position
        });
    };
}

function _windowsPositionStatusHandler(dispatch) {
    return positionStatus => {
        let status = positionStatus.status;
        if (status !== Windows.Devices.Geolocation.PositionStatus.ready) {
            const err = new EnhancedPositionError({ code: status.code, message: 'Position Error' });
            dispatch({
                type: POSITION_ERROR,
                positionError: err
            });
        }
    };
}

function startWatchingPosition() {
    return async (dispatch, getState) => {
        return new Promise(success => {
            let geolocationState = getState().geolocation;
            if (isNativeWindows()) {
                initWindowsDeviceLocator(geolocationState.settings);
                if (!windowsPositionWatching) {
                    windowsPositionChangedHandler = _windowsPositionChangedHandler(dispatch);
                    windowsPositionStatusHandler = _windowsPositionStatusHandler(dispatch);
                    windowsDevicesLocator.addEventListener('positionchanged', windowsPositionChangedHandler);
                    windowsDevicesLocator.addEventListener('statuschanged', windowsPositionStatusHandler);
                    windowsPositionWatching = true;
                }
            } else {
                if (positionWatchingHandle === null) {
                    positionWatchingHandle = navigator.geolocation.watchPosition(watchPositionHandler(dispatch), watchPositionErrorHandler(dispatch), geolocationState.settings);
                    dispatch({
                        type: WATCHING_POSITION_START
                    });
                }
            }
            success();
        });
    };
}



function stopWatchingPosition() {
    return async dispatch => {
        return new Promise(success => {
            if (isNativeWindows()) {
                if (windowsPositionWatching) {
                    windowsDevicesLocator.removeEventListener('positionchanged', windowsPositionChangedHandler);
                    windowsDevicesLocator.removeEventListener('statuschanged', windowsPositionStatusHandler);
                    windowsPositionChangedHandler = null;
                    windowsPositionStatusHandler = null;
                    windowsPositionWatching = false;
                }
            } else {
                if (positionWatchingHandle !== null) {
                    navigator.geolocation.clearWatch(positionWatchingHandle);
                    positionWatchingHandle = null;
                    dispatch({
                        type: WATCHING_POSITION_STOP
                    });
                }
            }
            success();
        });
    };
}

function restoreDefaultSettingsAction() {
    return {
        type: RESET_SETTINGS
    };
}

function isNumber(val) {
    return typeof val === 'number' && !isNaN(val);
}

function isPositiveNumber(val) {
    return isNumber(val) && val > 0;
}

function updateSettingsAction(settings, currentSettings) {
    let _settings;
    if (isNativeWindows()) {
        let { reportInterval, movementThreshold, desiredAccuracy } = settings;
        _settings = {
            reportInterval: isPositiveNumber(reportInterval) ? reportInterval : currentSettings.reportInterval,
            movementThreshold: isPositiveNumber(movementThreshold) ? movementThreshold : currentSettings.movementThreshold,
            desiredAccuracy: desiredAccuracy === Windows.Devices.Geolocation.PositionAccuracy.high || desiredAccuracy === Windows.Devices.Geolocation.PositionAccuracy.low ? desiredAccuracy : currentSettings.desiredAccuracy
        };
    } else {
        let { enableHighAccuracy, timeout, maximumAge } = settings;
        _settings = {
            enableHighAccuracy: typeof enableHighAccuracy === 'boolean' ? enableHighAccuracy : currentSettings.enableHighAccuracy,
            timeout: isPositiveNumber(timeout) ? timeout : currentSettings.timeout,
            maximumAge: isPositiveNumber(maximumAge) ? maximumAge : currentSettings.maximumAge,
        };
    }
    return {
        type: UPDATE_SETTINGS,
        settings: _settings
    };
}


function updateGeolocationSettings(settings) {
    return async (dispatch, getState) => {
        let geolocationState = getState().geolocation;
        let nextSettings = updateSettingsAction(settings, geolocationState.settings);
        if (geolocationState.watchingPosition) {
            await dispatch(stopWatchingPosition());
            dispatch(nextSettings);
            await dispatch(startWatchingPosition());
        } else {
            dispatch(nextSettings);
        }
    };
}

function restoreDefaultGeolocationSettings() {
    return async (dispatch, getState) => {
        let geolocationState = getState().geolocation;
        let defaultSettings = restoreDefaultSettingsAction();
        if (geolocationState.watchingPosition) {
            await dispatch(stopWatchingPosition());
            dispatch(defaultSettings);
            await dispatch(startWatchingPosition());
        } else {
            dispatch(defaultSettings);
        }
    };
}


export { updatePosition, startWatchingPosition, stopWatchingPosition, updateGeolocationSettings, restoreDefaultGeolocationSettings };