const NS = 'GEOLOCATION';

const POSITION_UPDATED = `${NS}@POSITION_UPDATED`;
const POSITION_UPDATING = `${NS}@POSITION_UPDATING`;
const POSITION_ERROR = `${NS}@POSITION_ERROR`;
const WATCHING_POSITION_START = `${NS}@WATCHING_POSITION_START`;
const WATCHING_POSITION_STOP = `${NS}@WATCHING_POSITION_STOP`;
const UPDATE_SETTINGS = `${NS}@UPDATE_SETTINGS`;
const RESET_SETTINGS = `${NS}@RESET_SETTINGS`;

export { POSITION_UPDATED, POSITION_UPDATING, POSITION_ERROR, WATCHING_POSITION_START, WATCHING_POSITION_STOP, UPDATE_SETTINGS, RESET_SETTINGS };