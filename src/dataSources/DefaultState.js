import { isNativeWindows } from '../isWindows';

const NULL_POSITION = {
    coords: {
        accuracy: 100000,
        altitude: null,
        altitudeAccuracy: null,
        heading: null,
        latitude: 0,
        longitude: 0,
        speed: null
    },
    timestamp: 0
};

const WINDOWS_DEVICES_MOVEMENT_THRESHOLD = 3; //meters
const WATCH_POSITION_TIMEOUT = 10000;

let DEFAULT_GEOLOCATION_SETTINGS = {
    enableHighAccuracy: true,
    timeout: WATCH_POSITION_TIMEOUT,
    maximumAge: 30 * 1000
};

let IS_WINDOWS = isNativeWindows();
if (IS_WINDOWS) {
    DEFAULT_GEOLOCATION_SETTINGS = {
        reportInterval: WATCH_POSITION_TIMEOUT,
        movementThreshold: WINDOWS_DEVICES_MOVEMENT_THRESHOLD,
        desiredAccuracy: Windows.Devices.Geolocation.PositionAccuracy.high
    };
}

//Default State
const STATE = {
    currentPosition: NULL_POSITION,
    positionError: null,
    updatingPosition: false,
    watchingPosition: false,
    settings: DEFAULT_GEOLOCATION_SETTINGS
};

export default STATE;