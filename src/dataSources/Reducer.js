import { reduceWith } from 'redux-reduce-with';
import DefaultState from './DefaultState';
import {
  POSITION_UPDATED,
  POSITION_UPDATING,
  POSITION_ERROR,
  WATCHING_POSITION_START,
  WATCHING_POSITION_STOP,
  UPDATE_SETTINGS,
  RESET_SETTINGS
} from './Actions';

const mutators = {
  [POSITION_UPDATED]: {
    currentPosition: action => action.currentPosition,
    updatingPosition: false,
    positionError: null
  },
  [POSITION_UPDATING]: {
    updatingPosition: true
  },
  [POSITION_ERROR]: {
    // currentPosition: DefaultState.currentPosition,
    updatingPosition: false,
    positionError: action => action.positionError
  },
  [WATCHING_POSITION_START]: {
    watchingPosition: true
  },
  [WATCHING_POSITION_STOP]: {
    watchingPosition: false
  },
  [UPDATE_SETTINGS]: {
    settings: action => action.settings
  },
  [RESET_SETTINGS]: {
    settings: action => DefaultState.settings
  }
};

export default reduceWith(mutators, DefaultState);
