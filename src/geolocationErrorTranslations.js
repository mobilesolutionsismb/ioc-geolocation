const IS_WINDOWS = typeof cordova !== 'undefined' && cordova.platformId === 'windows' && typeof Windows !== 'undefined';

function defaultTraslate(key, defaultText) {
    return defaultText;
}

function geolocationErrorTranslations(positionError, translate = defaultTraslate) {

    function _getPositionErrorReasonWindows(code) {
        let reason = '';
        switch (code) {
            //   case Windows.Devices.Geolocation.PositionStatus.ready:
            //    // Location data is available
            //    reason = translate('win_position_ready','Location is available.');
            //    break;
            case Windows.Devices.Geolocation.PositionStatus.initializing:
                // This status indicates that a GPS is still acquiring a fix
                reason = translate('_win_position_error_initializing', 'A GPS device is still initializing');
                break;
            case Windows.Devices.Geolocation.PositionStatus.noData:
                // No location data is currently available
                reason = translate('_win_position_error_noData', 'Data from location services is currently unavailable');
                break;
            case Windows.Devices.Geolocation.PositionStatus.disabled:
                // The app doesn't have permission to access location,
                // either because location has been turned off.
                reason = translate('_win_position_error_disabled', 'Your location is currently turned off. ' +
                    'Change your settings through the Settings charm ' +
                    ' to turn it back on');
                break;
            case Windows.Devices.Geolocation.PositionStatus.notInitialized:
                // This status indicates that the app has not yet requested
                // location data by calling GetGeolocationAsync() or
                // registering an event handler for the positionChanged event.
                reason = translate('_win_position_error_notInitialized', 'Location status is not initialized because ' +
                    'the app has not requested location data');
                break;
            case Windows.Devices.Geolocation.PositionStatus.notAvailable:
                // Location is not available on this version of Windows
                reason = translate('_win_position_error_notAvailable', 'You do not have the required location services ' +
                    'present on your system');
                break;
            default:
                reason = translate('_win_position_error_unknown', 'Unknown Position Status');
                break;
        }
        return reason;
    }

    function _getPositionErrorReason(positionError) {
        let {code, message} = positionError;
        message = message.length > 0 ? message: null;
        let reason = '';
        switch (code) {
            case 1: //PERMISSION DENIED
                reason = translate('_position_error_reason_permission', message || 'Permission denied. Check location access permission is enabled in your system settings. On Google Chrome, also check that your origin is secure (https://)');
                break;
            case 2: //POSITION UNAVAILABLE
                reason = translate('_position_error_reason_pos_unavailable', message || 'Position unavailable. Ensure your location sensor is enabled or try to move to a different place.');
                break;
            case 3: //TIMEOUT
                reason = translate('_position_error_reason_timeout', message || 'Position detection timeout.');
                break;
            default:
                reason = translate('_position_error_reason_default', message || 'Ensure your location sensor is enabled and/or you are connected to the internet.');
                break;
        }
        return reason;
    }

    //Windows devices
    if (IS_WINDOWS) {
        return _getPositionErrorReasonWindows(positionError.code);
    } else { //non windows or browser
        return _getPositionErrorReason(positionError);
    }

}

export { geolocationErrorTranslations };
export default geolocationErrorTranslations;