import geolocationErrorTranslations from './geolocationErrorTranslations';
import Reducer from './dataSources/Reducer';
import {
  default as withGeolocation,
  geolocationActionCreators,
  geolocationSelector
} from './dataProviders/withGeolocation';

if (!navigator.geolocation) {
  throw new Error('Your browser/device does not support geolocation');
}

import { PropTypes } from 'prop-types';
const coordsShape = {
  accuracy: PropTypes.number,
  altitude: PropTypes.number,
  altitudeAccuracy: PropTypes.number,
  heading: PropTypes.number,
  latitude: PropTypes.number,
  longitude: PropTypes.number,
  speed: PropTypes.number
};

const GeolocationShape = {
  coords: PropTypes.shape(coordsShape),
  timestamp: PropTypes.number
};

//Useful for type checking
const GeolocationType = PropTypes.shape(GeolocationShape);

export default {
  name: 'geolocation',
  reducer: Reducer
};

export * from './dataSources/ActionCreators';
export {
  Reducer,
  withGeolocation,
  geolocationErrorTranslations,
  GeolocationType,
  geolocationActionCreators,
  geolocationSelector
};
