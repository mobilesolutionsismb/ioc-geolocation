function isNativeWindows() {
    return typeof cordova !== 'undefined' && cordova.platformId === 'windows' && typeof Windows !== 'undefined';
}

export { isNativeWindows };
export default isNativeWindows;