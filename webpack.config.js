const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const pkg = require('./package.json');

const ENVIRONMENT = process.env.NODE_ENV || 'development';
const IS_PRODUCTION = ENVIRONMENT === 'production';

const OUTPUT_PATH = path.join(__dirname, 'dist');

const DEFINITIONS = new webpack.DefinePlugin({
    TITLE: JSON.stringify(pkg.title),
    VERSION: JSON.stringify(pkg.version),
    PKG_NAME: JSON.stringify(pkg.name),
    DESCRIPTION: JSON.stringify(pkg.description),
    ENVIRONMENT: JSON.stringify(ENVIRONMENT),
    IS_PRODUCTION: JSON.stringify(IS_PRODUCTION),
    BUILD_DATE: JSON.stringify(new Date()),
    //Used by ReactJS to turn optimization on/off
    'process.env': {
        NODE_ENV: JSON.stringify(ENVIRONMENT)
    }
});



const LOADER_OPTIONS = new webpack.LoaderOptionsPlugin({
    minimize: IS_PRODUCTION,
    debug: !IS_PRODUCTION,
    options: {
        context: __dirname
    }
});

let plugins = [
    DEFINITIONS,
    LOADER_OPTIONS
];


const entries = {
    index: [
        path.join(__dirname, 'src', 'index.js')
    ]
};

const JS_LOADERS = ['babel-loader'];
if (ENVIRONMENT === 'production') {
    JS_LOADERS.push(
        'strip-loader?strip[]=console.debug,strip[]=console.log'
    );
    plugins.push(new TerserPlugin({
        terserOptions: { ecma: 8 },
        sourceMap: IS_PRODUCTION
      }));
}

const mode = IS_PRODUCTION? 'production' : 'development';


let webpackConfig = {
    mode,
    context: path.join(__dirname, 'src'),
    entry: entries,
    devServer: {
        host: '0.0.0.0', //Any network interface
    },
    output: {
        path: OUTPUT_PATH,
        library: pkg.name,
        libraryTarget: 'umd',
        filename: IS_PRODUCTION ? '[name].min.js' : '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                use: JS_LOADERS
            }
        ]
    },
    resolve: {
        extensions: [
            '.js', // automatically in webpack 2
            '.jsx',
            '.json' // automatically in webpack 2
        ],
        modules: [
            'node_modules', path.resolve(__dirname, './node_modules'),
            'src'
        ]
    },
    externals: {
        '@mapbox/mapbox-gl-draw': '@mapbox/mapbox-gl-draw',
        '@turf/area': '@turf/area',
        'is-pojo': 'is-pojo',
        'localforage': 'localforage',
        'mapbox-gl': 'mapbox-gl',
        'material-ui': 'material-ui',
        'nop': 'nop',
        'prop-types':'prop-types',
        'react': 'react',
        'react-dom': 'react-dom',
        'react-mapbox-gl': 'react-mapbox-gl',
        'react-motion': 'react-motion',
        'react-redux': 'react-redux',
        'react-router-redux': 'react-router-redux',
        'react-tap-event-plugin': 'react-tap-event-plugin',
        'redux': 'redux',
        'redux-persist': 'redux-persist',
        'redux-reduce-with':'redux-reduce-with',
        'redux-thunk': 'redux-thunk',
        'reselect': 'reselect'
    },
    plugins: plugins,
    devtool: 'source-map'
};

module.exports = webpackConfig;